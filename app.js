var express = require('express');
var { graphiqlExpress, graphqlExpress } = require('graphql-server-express');
var bodyParser = require('body-parser');
var cors = require('cors');
var health = require('./src/api/health.js');
const { PORT } = require('./config');

var Schema = require('./schema.js');
var app = express();
health(app);
app.use(cors());

app.use(
    '/graphql',
    bodyParser.json(),
    graphqlExpress(req => ({
        schema: Schema,
        context: { token: req.headers.authorization }
    }))
);
app.use(
    '/graphiql',
    graphiqlExpress({
        endpointURL: '/graphql'
    })
);

app.listen(PORT, function() {
    console.log('Running on', PORT);
});

process.on('uncaughtException', function (error) {
    console.error(error);
});
