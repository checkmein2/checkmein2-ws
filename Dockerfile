FROM node:7.7.3-alpine

RUN mkdir /app

WORKDIR /app

COPY . /app

RUN yarn
ENV WS_PORT 8100
EXPOSE 8100

CMD [ "yarn", "start" ]

