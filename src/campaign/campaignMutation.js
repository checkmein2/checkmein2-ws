const {
    GraphQLInt,
    GraphQLNonNull,
    GraphQLString,
    GraphQLID
} = require('graphql');
const {
    campaign,
    campaignInput
} = require('./campaignSchema');
const sendReq = require('../utils/http');
const {
    API_GATEWAY_PORT,
    API_GATEWAY_HOST
} = require('../../config');

const createCampaign = {
    type: campaign,
    args: {
        campaign: {
            type: new GraphQLNonNull(campaignInput),
            description: 'Campaign name'
        }
    },
    resolve: function(rootValue, args, context, info) {
        return sendReq({
            hostname: API_GATEWAY_HOST,
            port: API_GATEWAY_PORT,
            path: '/campaigns',
            method: 'POST',
            args,
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                authorization: context.token
            }
        });
    }
};
const updateCampaign = {
    type: campaign,
    args: {
        campaign_id: {
            type: new GraphQLNonNull(GraphQLID),
            description: 'The campaign ID for the desired campaign'
        },
        campaign: {
            type: new GraphQLNonNull(campaignInput),
            description: 'Campaign name'
        }
    },
    resolve: function(rootValue, args, context, info) {
        const { campaign_id } = args;
        return sendReq({
            hostname: API_GATEWAY_HOST,
            port: API_GATEWAY_PORT,
            path: `/campaigns/${campaign_id}`,
            method: 'PUT',
            args,
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                authorization: context.token
            }
        });
    }
};
const removeCampaign = {
    type: campaign,
    args: {
        campaign_id: {
            type: new GraphQLNonNull(GraphQLID),
            description: 'The user ID for the desired user'
        }
    },
    resolve: function(rootValue, args, context, info) {
        const { campaign_id } = args;
        return sendReq({
            hostname: API_GATEWAY_HOST,
            port: API_GATEWAY_PORT,
            path: `/campaigns/${campaign_id}`,
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                authorization: context.token
            }
        });
    }
};
module.exports = {
    createCampaign,
    updateCampaign,
    removeCampaign
};
