const {
    GraphQLList,
    GraphQLNonNull,
    GraphQLID
} = require('graphql');
const { campaign } = require('./campaignSchema');
const sendReq = require('../utils/http');
const {
    API_GATEWAY_PORT,
    API_GATEWAY_HOST
} = require('../../config');

const getAllCampaigns = {
    type: new GraphQLList(campaign),
    resolve: function(rootValue, args, context, info) {
        return sendReq({
            hostname: API_GATEWAY_HOST,
            port: API_GATEWAY_PORT,
            path: '/campaigns',
            headers: {
                authorization: context.token
            }
        });
    }
};
const getCampaignById = {
    type: campaign,
    args: {
        campaign_id: {
            type: new GraphQLNonNull(GraphQLID),
            description: 'The user ID for the desired user'
        }
    },
    resolve: function(rootValue, args, context, info) {
        const { campaign_id } = args;
        return sendReq({
            hostname: API_GATEWAY_HOST,
            port: API_GATEWAY_PORT,
            path: `/campaigns/${campaign_id}`,
            headers: {
                authorization: context.token
            }
        });
    }
};

const getCampaignsByUser = {
    type: new GraphQLList(campaign),
    args: {
        user_id: {
            type: new GraphQLNonNull(GraphQLID),
            description: 'The user ID for the desired user'
        }
    },
    resolve: function(rootValue, args, context, info) {
        const { user_id } = args;
        return sendReq({
            hostname: API_GATEWAY_HOST,
            port: API_GATEWAY_PORT,
            path: `/campaigns/user/${user_id}`,
            headers: {
                authorization: context.token
            }
        });
    }
};

module.exports = {
    getAllCampaigns,
    getCampaignById,
    getCampaignsByUser
};
