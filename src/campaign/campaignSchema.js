const {
    GraphQLInputObjectType,
    GraphQLObjectType,
    GraphQLString,
    GraphQLInt,
    GraphQLFloat,
    GraphQLBoolean
} = require('graphql');

const campaign = new GraphQLObjectType({
    name: 'campaign',
    description: 'Represent the type of a campaign',
    fields: () => ({
        _id: { type: GraphQLString },
        name: { type: GraphQLString },
        content: { type: GraphQLString },
        user_id: { type: GraphQLString },
        startDate: { type: GraphQLFloat },
        endDate: { type: GraphQLFloat },
        media_id: { type: GraphQLString },
        summary: { type: GraphQLString },
        deadline_type: { type: GraphQLInt },
        deadline_run_for: { type: GraphQLInt },
        is_launched: { type: GraphQLBoolean }
    })
});

const campaignInput = new GraphQLInputObjectType({
    name: 'campaignInput',
    description: 'Represent the type of a campaign',
    fields: () => ({
        name: { type: GraphQLString },
        content: { type: GraphQLString },
        user_id: { type: GraphQLString },
        startDate: { type: GraphQLFloat },
        endDate: { type: GraphQLFloat },
        media_id: { type: GraphQLString },
        summary: { type: GraphQLString },
        deadline_type: { type: GraphQLInt },
        deadline_run_for: { type: GraphQLInt },
        is_launched: { type: GraphQLBoolean }
    })
});

module.exports = {
    campaign,
    campaignInput
};
