const {
    GraphQLList,
    GraphQLNonNull,
    GraphQLString,
    GraphQLObjectType,
    GraphQLID
} = require('graphql');

const {
    challengeType,
    checkInResponseType,
    checkInInputType,
    challengeInputType,
    deleteResponseType,
    participantResponseType
} = require('./challengeSchema');

const sendReq = require('../utils/http');

const { API_GATEWAY_PORT, API_GATEWAY_HOST } = require('../../config');

const checkIn = {
    type: checkInResponseType,
    args: {
        checkin: {
            type: new GraphQLNonNull(checkInInputType)
        }
    },
    resolve: function(rootValue, args, context) {
        return sendReq({
            hostname: API_GATEWAY_HOST,
            port: API_GATEWAY_PORT,
            path: '/challenges/checkin',
            method: 'POST',
            args,
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                authorization: context.token
            }
        });
    }
};

const createChalenge = {
    type: challengeType,
    args: {
        challenge: {
            type: new GraphQLNonNull(challengeInputType)
        }
    },
    resolve: function(rootValue, args, context) {
        return sendReq({
            hostname: API_GATEWAY_HOST,
            port: API_GATEWAY_PORT,
            path: '/challenges',
            method: 'POST',
            args,
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                authorization: context.token
            }
        });
    }
};

const updateChalenge = {
    type: challengeType,
    args: {
        challenge: {
            type: new GraphQLNonNull(challengeInputType)
        },
        challenge_id: {
            type: new GraphQLNonNull(GraphQLID),
            description: 'Challenge id we want to remove'
        }
    },
    resolve: function(rootValue, args, context) {
        return sendReq({
            hostname: API_GATEWAY_HOST,
            port: API_GATEWAY_PORT,
            path: `/challenges/challenge/${args.challenge_id}`,
            method: 'PUT',
            args,
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                authorization: context.token
            }
        });
    }
};

const addParticipant = {
    type: participantResponseType,
    args: {
        challenge_id: {
            type: new GraphQLNonNull(GraphQLID),
            description: 'Challenge id we add participant to'
        }
    },
    resolve: function(rootValue, args, context) {
        return sendReq({
            hostname: API_GATEWAY_HOST,
            port: API_GATEWAY_PORT,
            path: `/challenges/${args.challenge_id}/participant`,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                authorization: context.token
            }
        });
    }
};

const leaveChallenge = {
    type: participantResponseType,
    args: {
        challenge_id: {
            type: new GraphQLNonNull(GraphQLID),
            description: 'Challenge id from which we unsubscribe'
        }
    },
    resolve: function(rootValue, args, context) {
        return sendReq({
            hostname: API_GATEWAY_HOST,
            port: API_GATEWAY_PORT,
            path: `/challenges/${args.challenge_id}/participant`,
            method: 'DELETE',
            headers: {
                'Context-Type': 'application/json',
                Accept: 'application/json',
                authorization: context.token
            }
        });
    }
};

const deleteChallenge = {
    type: deleteResponseType,
    args: {
        challenge_id: {
            type: new GraphQLNonNull(GraphQLID),
            description: 'Challenge id we want to remove'
        }
    },
    resolve: function(rootValue, args, context) {
        return sendReq({
            hostname: API_GATEWAY_HOST,
            port: API_GATEWAY_PORT,
            path: `/challenges/challenge/${args.challenge_id}`,
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
                authorization: context.token
            }
        });
    }
};

module.exports = {
    checkIn,
    createChalenge,
    updateChalenge,
    deleteChallenge,
    addParticipant,
    leaveChallenge
};
