const {
    GraphQLObjectType,
    GraphQLInputObjectType,
    GraphQLNonNull,
    GraphQLList,
    GraphQLString,
    GraphQLInt,
    GraphQLID,
    GraphQLFloat,
    GraphQLBoolean
} = require('graphql');

const sponsorType = new GraphQLObjectType({
    name: 'SponsorType',
    description: 'Sponsor attached to a challenge',
    fields: () => ({
        _id: { type: GraphQLString },
        user_id: { type: GraphQLString },
        description: { type: GraphQLString }
    })
});
const sponsorInputType = new GraphQLInputObjectType({
    name: 'SponsorInputType',
    description: 'Input sponsor attached to a challenge',
    fields: () => ({
        description: { type: GraphQLString }
    })
});

const participantType = new GraphQLObjectType({
    name: 'ParticipantType',
    description: 'Participant attached to a challenge',
    fields: () => ({
        _id: { type: GraphQLString },
        user_id: { type: GraphQLString },
        checked_in: { type: GraphQLBoolean }
    })
});
const participantInputType = new GraphQLInputObjectType({
    name: 'ParticipantInputType',
    description: 'Input participant attached to a challenge',
    fields: () => ({
        _id: { type: GraphQLString },
        user_id: { type: GraphQLString },
        checked_in: { type: GraphQLBoolean }
    })
});

const location = new GraphQLInputObjectType({
    name: 'Location',
    description: 'Location object',
    fields: () => ({
        lat: { type: GraphQLString },
        lng: { type: GraphQLString }
    })
});

const deleteResponseType = new GraphQLObjectType({
    name: 'deleteResponseType',
    description: 'response from delete call',
    fields: () => ({
        _id: { type: GraphQLString },
        success: { type: GraphQLBoolean }
    })
});

const participantResponseType = new GraphQLObjectType({
    name: 'participantResponseType',
    description: 'response type for participant add/delete',
    fields: () => ({
        _id: { type: GraphQLString },
        user_id: { type: GraphQLString }
    })
});

const checkInType = new GraphQLObjectType({
    name: 'CheckInType',
    description: 'Represent the checkin options',
    fields: () => ({
        photo: { type: GraphQLBoolean },
        numeric: { type: GraphQLInt },
        lat: { type: GraphQLFloat },
        lng: { type: GraphQLFloat }
    })
});

const checkInTypeInput = new GraphQLInputObjectType({
    name: 'CheckInTypeInput',
    description: 'Represent the checkin options attached to a challenge',
    fields: () => ({
        photo: { type: GraphQLBoolean },
        numeric: { type: GraphQLInt },
        lat: { type: GraphQLFloat },
        lng: { type: GraphQLFloat }
    })
});

const challengeType = new GraphQLObjectType({
    name: 'ChallengeType',
    description: 'Represent the type of a challenge',
    fields: () => ({
        _id: { type: GraphQLString },
        campaign_id: { type: GraphQLString },
        name: { type: GraphQLString },
        owner_id: { type: GraphQLString },
        description: { type: GraphQLString },
        media_id: { type: GraphQLString },
        checkin_types: { type: checkInType },
        sponsors: { type: new GraphQLList(sponsorType) },
        participants: { type: new GraphQLList(participantType) }
    })
});

const challengeInputType = new GraphQLInputObjectType({
    name: 'ChallengeInputType',
    description: 'Represent the input type for a challenge',
    fields: {
        campaign_id: { type: new GraphQLNonNull(GraphQLString) },
        description: { type: new GraphQLNonNull(GraphQLString) },
        name: { type: new GraphQLNonNull(GraphQLString) },
        media_id: { type: GraphQLString },
        sponsor: { type: sponsorInputType },
        checkin_types: { type: checkInTypeInput }
    }
});

const checkInDataType = new GraphQLInputObjectType({
    name: 'checkInDataType',
    description: 'Represent the checkin data object attached to a checkin object',
    fields: () => ({
        photo: { type: GraphQLString },
        numeric: { type: GraphQLInt },
        lat: { type: GraphQLFloat },
        lng: { type: GraphQLFloat }
    })
});

const checkInInputType = new GraphQLInputObjectType({
    name: 'checkInInputType',
    description: 'Input data on checkin action',
    fields: {
        challenge_id: { type: new GraphQLNonNull(GraphQLID) },
        checkin_data: { type: checkInDataType }
    }
});
const checkInResponseType = new GraphQLObjectType({
    name: 'checkInResponseType',
    description: 'Response for checkin action',
    fields: {
        _id: { type: GraphQLID },
        completed: { type: new GraphQLNonNull(GraphQLBoolean) },
        message: { type: GraphQLString }
    }
});

module.exports = {
    challengeType,
    challengeInputType,
    deleteResponseType,
    participantResponseType,
    checkInInputType,
    checkInResponseType
};
