const {
    GraphQLList,
    GraphQLID,
    GraphQLNonNull,
    GraphQLString,
    GraphQLObjectType
} = require('graphql');
const { challengeType } = require('./challengeSchema');
const sendReq = require('../utils/http');
const { API_GATEWAY_PORT, API_GATEWAY_HOST } = require('../../config');

const getAllChallenges = {
    type: new GraphQLList(challengeType),
    resolve: (rootValue, args, context, info) =>
        sendReq({
            hostname: API_GATEWAY_HOST,
            port: API_GATEWAY_PORT,
            path: '/challenges'
        })
};

const getChallengesForCampaign = {
    type: new GraphQLList(challengeType),
    args: {
        campaign_id: {
            type: new GraphQLNonNull(GraphQLID)
        }
    },
    resolve: (rootValue, args, context, info) =>
        sendReq({
            hostname: API_GATEWAY_HOST,
            port: API_GATEWAY_PORT,
            path: `/challenges/${args.campaign_id}`
        })
};

const challengeById = {
    type: challengeType,
    args: {
        challenge_id: {
            type: new GraphQLNonNull(GraphQLID)
        }
    },
    resolve: (rootValue, args, context, info) =>
        sendReq({
            hostname: API_GATEWAY_HOST,
            port: API_GATEWAY_PORT,
            path: `/challenges/challenge/${args.challenge_id}`
        })
};

module.exports = {
    getAllChallenges,
    getChallengesForCampaign,
    challengeById
};
