const {
    GraphQLList,
    GraphQLNonNull,
    GraphQLID,
    GraphQLObjectType
} = require('graphql');
const { user } = require('./userSchema');
const sendReq = require('../utils/http');
const {
    API_GATEWAY_PORT,
    API_GATEWAY_HOST
} = require('../../config');


const getCurrentUser = {
    type: user,
    resolve: function(parent, args, context, info) {
        if (!context.token) {
            return {};
        }
        return sendReq({
            hostname: API_GATEWAY_HOST,
            port: API_GATEWAY_PORT,
            path: '/users/currentUser',
            headers: {
                authorization: context.token
            }
        });
    }
};

module.exports = {
    getCurrentUser
};
