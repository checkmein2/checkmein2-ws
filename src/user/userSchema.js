const {
    GraphQLInputObjectType,
    GraphQLOutputObjectType,
    GraphQLObjectType,
    GraphQLString,
    GraphQLInt
} = require('graphql');

const user = new GraphQLObjectType({
    name: 'User',
    description: 'Represent the type of a user profile',
    fields: () => ({
        id: { type: GraphQLString },
        name: { type: GraphQLString },
        email: { type: GraphQLString },
        user_role: { type: GraphQLString }
    })
});

const userLoginInput = new GraphQLInputObjectType({
    name: 'userLogin',
    description: 'Represent the type of a user login',
    fields: () => ({
        access_token: { type: GraphQLString },
        name: { type: GraphQLString },
        email: { type: GraphQLString },
        id: { type: GraphQLString }
    })
});
const adminLoginInput = new GraphQLInputObjectType({
    name: 'adminLogin',
    description: 'Represent the type of an admin login',
    fields: () => ({
        username: { type: GraphQLString },
        password: { type: GraphQLString }
    })
});

const userLoginOutput = new GraphQLObjectType({
    name: 'userLoginOutput',
    description: 'Represent the output type of a user login',
    fields: () => ({
        jwt: { type: GraphQLString }
    })
});

module.exports = {
    user,
    userLoginInput,
    userLoginOutput
};
