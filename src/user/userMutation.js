const {
    GraphQLInt,
    GraphQLNonNull,
    GraphQLString,
    GraphQLObjectType
} = require('graphql');
const {
    user,
    userLoginOutput,
    userLoginInput
} = require('./userSchema');
const sendReq = require('../utils/http');
const {
    API_GATEWAY_PORT,
    API_GATEWAY_HOST
} = require('../../config');

const userLogin = {
    type: userLoginOutput,
    args: {
        profile: {
            type: new GraphQLNonNull(userLoginInput)
        }        
    },
    resolve: function(rootValue, args, context, info) {
        return sendReq({
            hostname: API_GATEWAY_HOST,
            port: API_GATEWAY_PORT,
            path: '/auth/facebookLogin',
            method: 'POST',
            args,
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json'
            }
        });
    }
};
const adminLogin = {
    type: userLoginOutput,
    args: {
        username: {
            type: new GraphQLNonNull(GraphQLString),
            description: 'username'
        },
        password: {
            type: new GraphQLNonNull(GraphQLString),
            description: 'password'
        }
    },
    resolve: function(rootValue, args, context, info) {
        return sendReq({
            hostname: API_GATEWAY_HOST,
            port: API_GATEWAY_PORT,
            path: '/auth/admin/login',
            method: 'POST',
            args,
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json'
            }
        });
    }
};
module.exports = {
    userLogin,
    adminLogin
};
