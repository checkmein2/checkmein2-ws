const {
    GraphQLInputObjectType,
    GraphQLObjectType,
    GraphQLString,
    GraphQLInt
} = require('graphql');

const media = new GraphQLObjectType({
    name: 'media',
    description: 'Represent the type of a media',
    fields: () => ({
        _id: { type: GraphQLString }
    })
});

const photoType = new GraphQLInputObjectType({
    name: 'PhotoType',
    description: 'Photo type',
    fields: () => ({
        name: { type: GraphQLString },
        type: { type: GraphQLString },
        size: { type: GraphQLInt },
        path: { type: GraphQLString }
    })
});
const mediaInput = new GraphQLInputObjectType({
    name: 'mediaInput',
    description: 'Represent the type of a media',
    fields: () => ({
        fileName: { type: GraphQLString },
        photo: {
            type: photoType
        }
    })
});

module.exports = {
    media,
    mediaInput
};
