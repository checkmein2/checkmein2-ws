const {
    GraphQLInt,
    GraphQLNonNull,
    GraphQLString
} = require('graphql');
const fs = require('fs');
const {
    media,
    mediaInput
} = require('./mediaSchema');
const sendReq = require('../utils/http');
const {
    API_GATEWAY_PORT,
    API_GATEWAY_HOST
} = require('../../config');

const uploadPhoto = {
    type: media,
    args: {
        media: {
            type: new GraphQLNonNull(mediaInput),
            description: 'Upload photo'
        }
    },
    resolve: function(rootValue, args, context, info) {
        const filename = args.media.photo.name;
        const contentType = args.media.photo.type;
        const path = args.media.photo.path;
        return sendReq({
            hostname: API_GATEWAY_HOST,
            port: API_GATEWAY_PORT,
            path: '/media/photo',
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW',
                'cache-control': 'no-cache'
            },
            multipart: {
                chunked: false,
                data: [
                    {
                        'content-type': contentType,
                        body: fs.createReadStream(x)
                    }
                ]
            }
        });
    }
};
module.exports = {
    uploadPhoto
};
