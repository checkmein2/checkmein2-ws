const http = require('http');
module.exports = options => {
    return new Promise((resolve, reject) => {
        const httpCallBack = response => {
            let str = '';
            response.on('data', chunk => {
                str += chunk;
            });
            response.on('end', () => {
                const resp = JSON.parse(str);
                resolve(resp);
            });
        };
        const newRequest = http.request(options, httpCallBack);
        if (options.args) {
            newRequest.write(JSON.stringify(options.args));
        }
        newRequest.end();
    });
};
