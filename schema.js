const { GraphQLObjectType, GraphQLSchema, GraphQLString } = require('graphql');
const { CHALLENGE_SERVICE_PORT, SERVICE_HOST } = require('./config');

const sendReq = require('./src/utils/http');

const userQuery = require('./src/user/userQuery');
const userMutation = require('./src/user/userMutation');

const campaignQuery = require('./src/campaign/campaignQuery');
const campaignMutation = require('./src/campaign/campaignMutation');

const challengeQuery = require('./src/challenge/challengeQuery');
const challengeMutation = require('./src/challenge/challengeMutation');

const mediaMutation = require('./src/media/mediaMutation');

const Query = new GraphQLObjectType({
    name: 'RootQuery',
    fields: {
        currentUser: userQuery.getCurrentUser,
        campaigns: campaignQuery.getAllCampaigns,
        campaignById: campaignQuery.getCampaignById,
        campaignsByUser: campaignQuery.getCampaignsByUser,
        // challanges
        challenges: challengeQuery.getAllChallenges,
        challengeById: challengeQuery.challengeById,
        challengesForCampaign: challengeQuery.getChallengesForCampaign
    }
});

const Mutation = new GraphQLObjectType({
    name: 'Mutations',
    fields: {
        userLogin: userMutation.userLogin,
        adminLogin: userMutation.adminLogin,
        // campaigns
        createCampaign: campaignMutation.createCampaign,
        updateCampaign: campaignMutation.updateCampaign,
        removeCampaign: campaignMutation.removeCampaign,
        // challanges
        createChalenge: challengeMutation.createChalenge,
        updateChalenge: challengeMutation.updateChalenge,
        deleteChallenge: challengeMutation.deleteChallenge,
        addParticipant: challengeMutation.addParticipant,
        leaveChallenge: challengeMutation.leaveChallenge,
        //checkin
        checkIn: challengeMutation.checkIn,
        //media
        uploadPhoto: mediaMutation.uploadPhoto
    }
});

const Schema = new GraphQLSchema({
    query: Query,
    mutation: Mutation
});

module.exports = Schema;
